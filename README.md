[//]: <> (https://www.youtube.com/watch?v=CYqd2AHXosk)

# bread-software-repo

This is a software repo for my own packages.

add this to your /etc/pacman.conf

[bread-software-repo]
<br>
SigLevel = Optional DatabaseOptional
<br>
Server = https://gitlab.com/Breadleaf/$repo/-/raw/main/$arch
