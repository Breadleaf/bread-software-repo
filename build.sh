#!/usr/bin/bash

rm x86_64/*

dir="software-pre-formatted/"
array=($(ls ${dir}))
len=${#array[*]}

for ((i = 0; i < ${len}; i++));
do
	cd ${dir}${array[$i]} && makepkg -c && rm -rf ${array[$i]}/ && mv *.pkg.tar.zst ../../x86_64 && cd ../..
done

cd x86_64/ && repo-add bread-software-repo.db.tar.gz *.pkg.tar.zst && rm bread-software-repo.db bread-software-repo.files && mv bread-software-repo.db.tar.gz bread-software-repo.db && mv bread-software-repo.files.tar.gz bread-software-repo.files && cd ..

git add --all
git commit -m "software update"
git push
